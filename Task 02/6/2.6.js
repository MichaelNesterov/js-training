function myBind(fun, context) {
    return function() {
        return fun.apply(context, arguments);
    };
}

function sum(a, b) {
    return this.number + a + b;
}

var number = 0;
console.log(sum(10, 20)); // 30

var bindedSum = myBind(sum, {number: 100});
console.log(bindedSum(40, 50)); // 190