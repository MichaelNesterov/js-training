var generator = sequence(10, 3);

alert(generator()); // 10

alert(generator()); // 13

alert(generator()); // 16


function sequence(start, step) {
    return function () {
        return  (start += step) - step;
    }
}