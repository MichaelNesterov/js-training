var x = sum(2)(5)(10);

alert(x); // выводит 17

function sum(a) {

    var v_sum = a;

    function f(b) {
        v_sum += b;
        return f;
    }

    f.toString = function() {
        return v_sum;
    };

    return f;
}