console.log(checkRectangle(2,2,2,6,5,6,5,2));
console.log(checkDotInRectangle(2,2,1,1,1,3,3,3,3,1));
console.log(checkDotInRectangle(20,20,1,1,1,3,3,3,3,1));


function distanceToLine(dx0, dy0, x1, y1, x2, y2) {
    return Math.abs((y2-y1)*dx0 - (x2-x1)*dy0 + x2*y1 - y2*x1) / distanceBetweenDot(x1, y1, x2, y2);
}

function distanceBetweenDot(x1, y1, x2, y2) {
    return Math.hypot(x2-x1, y2-y1);
}

function checkStraightAngle(x1, y1, x2, y2, x3, y3) {
    return distanceToLine(x3, y3, x1, y1, x2, y2) === distanceBetweenDot(x2, y2, x3, y3);
}

function checkRectangle(x1, y1, x2, y2, x3, y3, x4, y4) {
    return  checkStraightAngle(x1, y1, x2, y2, x3, y3) &&
            checkStraightAngle(x2, y2, x3, y3, x4, y4) &&
            checkStraightAngle(x3, y3, x4, y4, x1, y1) &&
            checkStraightAngle(x4, y4, x1, y1, x2, y2);
}

function checkDotInRectangle(dx, dy, x1, y1, x2, y2, x3, y3, x4, y4) {
    var a1 =  checkSideOfVector(dx, dy, x1, y1, x2, y2);
    var a2 =  checkSideOfVector(dx, dy, x2, y2, x3, y3);
    var a3 =  checkSideOfVector(dx, dy, x3, y3, x4, y4);
    var a4 =  checkSideOfVector(dx, dy, x4, y4, x1, y1);

    return a1 <= 0 && a2 <= 0 && a3 <= 0 && a4 <= 0;
}

function checkSideOfVector(dx, dy, x1, y1, x2, y2) {
    var a = -(y2 - y1);
    var b = x2 - x1;
    var c = -(a * x1 + b * y1);

    return a * dx + b * dy + c;
}