plusOne = decoratorArgumentTypeNumber(plusOne);
plusOne(1);
plusOne(10);
plusOne('dfdf');

plusOne = decoratorCheckArgumentsType(plusOne, 'number');
plusOne(1,2,3);
plusOne(1,2,'dfdf');
plusOne(1,2, 6, null, 5);


function decoratorArgumentTypeNumber(fun) {
    return function () {
        if(typeof arguments[0] !== 'number') {
            alert( "Тип аргумента не number" );
            return;
        }
        return fun.apply(this, arguments);
    }
}

function decoratorCheckArgumentsType(fun, type) {
    return function () {
        for (var i = 0; i < arguments.length; i++) {
            if (typeof arguments[i] !== type) {
                alert( "Тип аргумента номер " + (i +1) + " не " + type);
                return;
            }
        }
        return fun.apply(this, arguments);
    }
}

function plusOne(a) {
    alert(a + 1);
}