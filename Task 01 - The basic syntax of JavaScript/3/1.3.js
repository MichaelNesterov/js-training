function checkInput(text) {
    var value = parseFloat(prompt(text));

    if (isNaN(value) || value <= 0 || value % 1 !== 0) {
        throw new SyntaxError("Данные некорректны");
    }

    return value;
}

try {
        var num = checkInput("Введите № числа Фибоначчи");

        if (num <= 2) {
            alert(1);
        } else {
            var x = 1;
            var y = 1;
            var ans = 0;
            for (var i = 2; i < num; i++)
            {
                ans = x + y;
                x = y;
                y = ans;
            }
            alert(ans);
        }
} catch (e) {
    alert(e);
}