function checkInput(text, maxValue) {
    var value = parseFloat(prompt(text));

    if (isNaN(value) || value <= 0 || value % 1 !== 0 || value > maxValue) {
        throw new SyntaxError("Данные некорректны");
    }

    return value;
}

function getWeekDay(date) {
    var days = ["воскресенье", "понедельник", "вторник", "среда", "четверг", "пятница", "суббота"];

    return days[date.getDay()];
}

var month = checkInput('Номер месяца', 12);
var day = checkInput('Номер дня', 31);
var date = new Date(2016, month-1, day);
alert( getWeekDay(date) );