function checkInput(text, maxValue) {
    var value = parseFloat(prompt(text));

    if (isNaN(value) || value <= 0 || value % 1 !== 0 || value > maxValue) {
        throw new SyntaxError("Данные некорректны");
    }

    return value;
}

try {
    var level = checkInput('Количество этажей', 25);

    var entrance = checkInput('Количество подъездов', 10);

    var flatCount = checkInput('Количество квартир на этаже', 20);

    var flat = checkInput('№ квартиры', flatCount * entrance * level);

    alert('№ подъезда: ' + Math.ceil(flat / (flatCount * level)));
} catch (e) {
    alert(e);
}

