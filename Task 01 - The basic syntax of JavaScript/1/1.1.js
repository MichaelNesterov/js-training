var a,b;

while (true) {
    a = parseFloat(prompt('Введите первое число'));

    if (isNaN(a)) {
        alert('первый ввод – не число');
        break;
    }

    b = parseFloat(prompt('Введите второе число'));

    if (isNaN(b)) {
        alert('второй ввод – не число');
        break;
    }

    if( a < b ){
        alert('первое число меньше');
    } else if( a > b ){
        alert('второе число меньше');
    } else {
        alert('числа равны');
    }
}