function add(number) {
    return function (number2) {
        return number += number2;
    }
}

function averageRec(array, sum = 0, i = 0) {
    if (i >= array.length) {
        return sum / array.length;
    }
    return averageRec(array, sum + array[i], ++i);
}


function standardDeviationRec(array, avg = averageRec(array), sum = 0, i = 0) {
    if (i >= array.length) {
        return Math.sqrt(sum / array.length);
    }
    return standardDeviationRec(array, avg, sum + (array[i] - avg) * (array[i] - avg), ++i);
}

function replaceCharRec(original, replace, string, i = 0, newString = '') {
    if (i === string.length) {
        return newString
    }
    if (string[i] === original) {
        newString += replace
    } else {
        newString += string[i];
    }
    return replaceCharRec(original, replace, string, ++i, newString);
}


function customSplitRec(string, separator, i = 0, j = 0, newString = []) {
    if (i === string.length) {
        return newString
    }
    if (string[i] === separator) {
        return customSplitRec(string, separator, ++i, ++j, newString);
    }
    if (!newString[j]) {
        newString[j] = '';
    }

    newString[j] += string[i];

    return customSplitRec(string, separator, ++i, j, newString);
}


function bubleSortRec(array, i = array.length - 1, j = 0) {
    if (i === 0) {
        return array;
    }
    if (j >= i) {
        return bubleSortRec(array, --i, 0);
    }
    if (array[j] > array[j + 1]) {
        var temp = array[j];
        array[j] = array[j + 1];
        array[j + 1] = temp;
    }

    return bubleSortRec(array, i, ++j);
}

var myF = add(6);
console.log(myF(2));

var array = [1, 2, 3, 4, 5];
console.log(averageRec(array));
console.log(standardDeviationRec(array));
console.log(replaceCharRec('1', '9', '123123123'));
console.log(customSplitRec('123123123', '1'));
console.log(bubleSortRec([1, 7, 3, 8, 5]));

