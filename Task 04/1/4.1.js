var v1 = new Vector(1,2,3);

console.log(v1.toString());
console.log(+v1);

function Vector(x, y, z) {
    this.x = x;
    this.y = y;
    this.z = z;

    this.plus = function (vector) {
        return new Vector(this.x + vector.x, this.y + vector.y, this.z + vector.z);
    };

    this.scalar = function (vector) {
        return this.x * vector.x + this.y * vector.y + this.z * vector.z;
    };

    this.toString = function () {
        return 'x: ' + this.x + ', y: ' + this.y + ', z: ' + this.z;
    };

    this.valueOf = function () {
        return Math.hypot(this.x, this.y, this.z);
    };
}
