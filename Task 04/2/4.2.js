var t1 = new Task('task1', '1111', '01.01.2018', '01.01.2019');
console.log(t1);

var t2 = new ExecutableTask('task1', '1111', '01.05.2018', '01.05.2019', 90, false);
console.log(t2);
console.log(ExecutableTask.prototype);
console.log(Task.prototype);


function Task(name, description, startDate, endDate, tasks = []) {

    this.name = name;
    this.description = description;
    this.startDate = startDate;
    this.endDate = endDate;
    this.tasks = tasks;
}

function ExecutableTask(name, description, startDate, endDate, progress, done, tasks = []) {

    Task.call(this, name, description, startDate, endDate, tasks);

    this.progress = progress;
    this.done = done;
}

ExecutableTask.prototype = Object.create(Task.prototype);
ExecutableTask.prototype.constructor = ExecutableTask;