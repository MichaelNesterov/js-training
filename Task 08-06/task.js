var string = prompt('Введите последовательность чисел');

var replaceString = replaceChar(',', '.', string);

var array = customSplit(replaceString, ' ');

var flag = true;

convertArray(array);

if(flag){
    alert("Исходный массив: " + string + '\n' +
        "Отсортированный массив: " + bubleSort(array) + '\n' +
        "Среднее: " + average(array) + '\n' +
        "Среднеквадратичное отклонение: " + standardDeviation(array) + '\n' +
        "Миминум: " + array[0] + '\n' +
        "Максимум: " + array[array.length-1] + '\n'
    );
}

function parseNumber(number) {
    var parsedNumber = parseFloat(number);

    if (!isNaN(parsedNumber) && isFinite(number)) {
        return parsedNumber;
    }
    return NaN;
}

function convertArray(array) {
    var i = array.length-1;
    for(; i >= 0; i--){
        var number = parseNumber(array[i]);
        if(isNaN(number)){
            alert('Неверный формат ввода');
            flag = false;
            return;
        }
        array[i] = number;
    }
}

function bubleSort(array) {
    var temp, j, i = array.length-1;
    for(; i > 0; i--){
        for(j = 0; j < i; j++){
            if( array[j] > array[j+1]) {
                temp = array[j];
                array[j] = array[j+1];
                array[j+1] = temp;
            }
        }
    }
    return array;
}

function average(array) {
    var i = array.length-1;
    var sum = 0;
    for(; i >= 0; i--){
        sum += array[i];
    }
    return sum/array.length;
}

function standardDeviation(array) {
    var avg = average(array);
    var i = array.length-1;
    var sum = 0;
    for(; i >= 0; i--){
        sum += (array[i] - avg) * (array[i] - avg);
    }
    return Math.sqrt(sum/array.length);
}

function replaceChar(original, replace, string) {
    var i = 0, length = string.length;
    var newString = '';
    for(; i < length; i++){
        if(string[i] === original) {
            newString += replace
        } else {
            newString += string[i];
        }
    }
    return newString;
}

function customSplit(string, separator) {
    var i = 0, j = 0;
    var newString = [];
    for(;i < string.length; i++){
        if(string[i] === separator) {
            j++;
            continue;
        }

        if(!newString[j]){
            newString[j] = '';
        }

        newString[j] += string[i];
    }

    return newString;
}

